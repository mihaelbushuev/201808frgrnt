<?php

	class Settings_model extends CI_Model {

		public function __construct() {

			$this->load->database();

		}

		public function get_settings() {

			$query = $this->db->get_where('settings', array('name' => 'price_choice'));
			$data = $query->row_array();
			return $data;

		}

		public function set_settings() {

			$data = array(
				'value' => $this->input->post('price_choice')
			);

			$this->db->where('name', 'price_choice');
			$this->db->update('settings', $data);
			return true;

		}

	}
