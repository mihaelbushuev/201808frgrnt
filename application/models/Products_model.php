<?php

	class Products_model extends CI_Model {

		public function __construct() {

			$this->load->database();

		}

		public function get_products($id = FALSE) {

			if ($id === FALSE) {
				$query = $this->db->get('products');
				$data = $query->result_array();
				foreach ($data as $key => $value) {
					$data_price = $this->get_product_price($value['id'], $value['base_price'], $value['price_choice']);
					$data[$key]['price'] = $data_price[0]['price'];
				}
			} else {
				$query = $this->db->get_where('products', array('id' => $id));
				$data = $query->row_array();
				$data_price = $this->get_product_price($id, $data['base_price'], $data['price_choice']);
				$data['price'] = $data_price[0]['price'];
			}


			return $data;

		}

		public function set_products($id = FALSE) {

			$data = array(
				'title' => $this->input->post('title'),
				'base_price' => $this->input->post('base_price'),
				'price_choice' => $this->input->post('price_choice')
			);

			if ($id === FALSE) {
				$this->load->helper('url');

				$id = url_title($this->input->post('title'), 'dash', TRUE);

				return $this->db->insert('products', $data);
			} else {
				$this->db->where('id', $id);
				$this->db->update('products', $data);
				return true;
			}

		}

		public function set_product_price($product_id = FALSE, $action = 'price_create') {

			if ( $action == 'price_create' ) {

				$data = array(
					'product_id' => $product_id,
					'price' => $this->input->post('price'),
					'period_begin' => $this->input->post('period_begin') . '00:00:00',
					'period_end' => $this->input->post('period_end') . '23:59:59',
					'type' => $this->input->post('type')
				);

				return $this->db->insert('prices', $data);
			}
			if ( $action == 'price_delete' ) {
				$id = $this->input->post('id');
				return $this->db->delete('prices', array('id' => $id));
			}

		}

		public function get_product_price($id = FALSE, $base_price = 0, $sort = 'default', $opt = 'single', $date = FALSE) {

			if ($id === FALSE) {
				return '0';
			}
			if ( !$date ) $date = date('Y-m-d 00:00:00');
			$cur_time = strtotime( $date );
			if ( $opt == 'single' ) {
				$begin_time = $to_time = $cur_time;
			}
			if ( $opt == 'period' ) {
				$begin_time = strtotime( '-7 days', $cur_time );
				// $to_time = strtotime( '+7 days', $cur_time );
				$to_time = $cur_time;
			}
			if ( in_array( $sort, array( 'min_period', 'latest' ) ) ) {
				$price_choice = $sort;
			} else {
				$price_choice = $this->get_settings_price_choice();
			}

			for( $time=$begin_time; $time<=$to_time; $time+=86400 ) {
				$date = date('Y-m-d 00:00:00', $time );
				$data_prices['date'] = $date;
				$data_prices_tmp = $this->products_model->get_product_prices($id, $price_choice, $date);
				$data_prices['price'] = ( isset( $data_prices_tmp[0]['price'] ) ? $data_prices_tmp[0]['price'] : $base_price );
				$data_prices_res[] = $data_prices;
			}

			return $data_prices_res;

		}

		public function get_product_prices($product_id = FALSE, $sort = 'default', $date = FALSE) {

			if ($product_id === FALSE) {
				return false;
			}
			if ( in_array( $sort, array( 'min_period', 'latest' ) ) ) {
				$price_choice = $sort;
			} else {
				$price_choice = $this->get_settings_price_choice();
			}
			if ($date !== FALSE) {
				$this->db->where('period_begin <=', $date);
				$this->db->where('period_end >=', date('Y-m-d 23:59:59', strtotime( $date ) ) );
				$this->db->limit(1);
			}

			if ( $price_choice == 'latest' ) {
				$this->db->order_by('id', 'DESC');
			} elseif ( $price_choice == 'min_period' ) {
				$this->db->order_by('period_begin', 'DESC');
				$this->db->order_by('period_end', 'ASC');
			}

			$query = $this->db->get_where('prices', array('product_id' => $product_id));
			return $query->result_array();

		}

		public function get_settings_price_choice() {
			$this->db->select('value');
			$res = $this->db->get_where('settings', array('name' => 'price_choice'));
			$row = $res->row();
			return $row->value;
		}

	}
