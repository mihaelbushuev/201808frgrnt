<?php

	class Settings extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->library('parser');
			$this->load->model('settings_model');

		}

		public function index() {
			$this->load->helper('form');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('price_choice', 'Price Choice Priority', 'required');

			$data['title'] = 'Settings';

			$this->parser->parse('templates/header', $data);

			if ($this->form_validation->run() === FALSE) {
				$res['mess'] = validation_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">', '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			}
			$action = $this->input->post('action');
			$name = $this->input->post('name');
			if ($this->form_validation->run() !== FALSE) {
				if ( $action == 'settings_edit' && $name == 'price_choice'  ) {
					$res['data'] = $this->settings_model->set_settings();
				}
				$res['mess'] = '
										<div class="alert alert-success alert-dismissible fade show" role="alert">
											Changes Saved
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
			}
			$this->parser->parse('admin/success', $res);

			$data['settings'] = $this->settings_model->get_settings();
			$data['settings_name'] = $data['settings']['name'];
			$data['settings_title'] = $data['settings']['title'];
			$data['price_choice'] = $data['settings']['value'];

			$this->parser->parse('admin/settings', $data);
			$this->parser->parse('templates/footer', $data);
		}

	}
