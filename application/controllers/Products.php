<?php

	class Products extends CI_Controller {

		public function __construct() {
			parent::__construct();
			$this->load->library('parser');
			$this->load->model('products_model');
		}

		public function index() {
			$data['products'] = $this->products_model->get_products();
			$data['title'] = 'Products Catalog';

			$this->parser->parse('templates/header', $data);
			$this->parser->parse('catalog/index', $data);
			$this->parser->parse('templates/footer', $data);
		}

		public function view($id = NULL) {
			$data['products_item'] = $this->products_model->get_products($id);
			$data['title'] = 'Product';

			if (empty($data['products_item'])) {
				show_404();
			}

			$data['prod_id'] = $data['products_item']['id'];
			$data['prod_title'] = $data['products_item']['title'];
			$data['prod_price'] = $data['products_item']['price'];

			$data['products_prices'] = $this->products_model->get_product_price( $id, $data['products_item']['base_price'], $data['products_item']['price_choice'], 'period' );

			$this->parser->parse('templates/header', $data);
			$this->parser->parse('catalog/view', $data);
			if ( ! empty($data['products_prices']) ) {
				$data['shieldChart'] = true;
				$this->parser->parse('catalog/prices', $data);
			}
			$this->parser->parse('templates/footer', $data);
		}

		public function create() {
			$this->load->helper('form');
			$this->load->library('form_validation');

			$data['title'] = 'Create a products item';

			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('base_price', 'Base Price', 'required');

			$this->parser->parse('templates/header', $data);
			if ($this->form_validation->run() === FALSE) {
				$this->parser->parse('admin/create', $data);
			} else {
				$this->products_model->set_products();
				$res['mess'] = '
										<div class="alert alert-success alert-dismissible fade show" role="alert">
											Product Added
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
				$this->parser->parse('admin/success', $res);
			}
			$this->parser->parse('templates/footer', $data);
		}

		public function edit($id = NULL) {
			$this->load->helper('form');
			$this->load->library('form_validation');

			$data['title'] = 'Edit Product';
			$this->parser->parse('templates/header', $data);

			$res['action'] = $this->input->post('action');
			if ( $res['action'] == 'product_edit'  ) {
				$this->form_validation->set_rules('title', 'Title', 'required');
				$this->form_validation->set_rules('base_price', 'Base Price', 'required');
				$this->form_validation->set_rules('price_choice', 'Price Choice Priority', 'required');
			}
			if ( in_array( $res['action'], array( 'price_create', 'price_edit', ) ) ) {
				$this->form_validation->set_rules('price', 'Price', 'required');
			}
			if ( in_array( $res['action'], array( 'price_delete', ) ) ) {
				$this->form_validation->set_rules('id', 'ID', 'required');
			}

			if ($this->form_validation->run() === FALSE) {
				$res['mess'] = validation_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">', '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			}
			if ($this->form_validation->run() !== FALSE) {
				if ( $res['action'] == 'product_edit'  ) {
					$res['data'] = $this->products_model->set_products($id);
				}
				if ( in_array( $res['action'], array( 'price_create', 'price_edit', 'price_delete', ) ) ) {
					$res['data'] = $this->products_model->set_product_price( $id, $res['action'] );
				}
				$res['mess'] = '
										<div class="alert alert-success alert-dismissible fade show" role="alert">
											Changes Saved
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>';
			}
			$this->parser->parse('admin/success', $res);

			$data['products_item'] = $this->products_model->get_products($id);

			if (empty($data['products_item'])) {
				show_404();
			}

			$data['prod_id'] = $data['products_item']['id'];
			$data['prod_title'] = $data['products_item']['title'];
			$data['prod_price'] = $data['products_item']['base_price'];
			$data['prod_price_choice'] = $data['products_item']['price_choice'];

			$this->parser->parse('admin/edit', $data);

			$data['products_prices'] = $this->products_model->get_product_prices($id, $data['products_item']['price_choice']);

			if ( ! empty($data['products_prices']) ) {
				$this->parser->parse('admin/prices', $data);
			}

			$this->parser->parse('templates/footer', $data);
		}

	}
