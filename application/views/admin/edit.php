	<!-- ITEM
	============================================= -->
	<section class="section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="card p-4">

						<?php echo form_open("admin/edit/$prod_id") ?>

							<input type="hidden" name="action" value="product_edit" />

							<div class="form-group">
								<label for="title">Title</label>
								<input class="form-control" type="input" name="title" value="{prod_title}" />
							</div>

							<div class="form-group">
								<label for="text">Base Price</label>
								<input class="form-control" type="input" name="base_price" value="{prod_price}" />
							</div>

							<div class="form-group">
								<label for="text">Price Choice Priority</label><br />
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="price_choice" value="default" id="price_choice_default" <?php echo ( $prod_price_choice == 'default' ? 'checked="checked"' : '' ); ?> />
									<label class="form-check-label" for="price_choice_default"> by default</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="price_choice" value="min_period" id="price_choice_min_period" <?php echo ( $prod_price_choice == 'min_period' ? 'checked="checked"' : '' ); ?> />
									<label class="form-check-label" for="price_choice_min_period"> by minimum period</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="price_choice" value="latest" id="price_choice_latest" <?php echo ( $prod_price_choice == 'latest' ? 'checked="checked"' : '' ); ?> />
									<label class="form-check-label" for="price_choice_latest"> by latest added</label>
								</div>
							</div>

							<div class="form-row">
								<input class="btn btn-primary" type="submit" name="submit" value="Save Changes" />
								<a class="btn btn-success ml-2" href="<?php echo site_url(); ?>{prod_id}">Back to Product</a>
								<a class="btn btn-secondary ml-2" href="<?php echo base_url(); ?>">Back to Catalog</a>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</section>
