	<!-- ITEM
	============================================= -->
	<section class="section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="card p-4">

						<?php echo validation_errors(); ?>

						<?php echo form_open('admin/create') ?>

							<div class="form-group">
								<label for="title">Title</label>
								<input class="form-control" type="input" name="title" />
							</div>

							<div class="form-group">
								<label for="text">Base Price</label>
								<input class="form-control" type="input" name="base_price" />
							</div>

							<div class="form-group">
								<label for="text">Price Choice Priority</label><br />
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="price_choice" value="default" id="price_choice_default" checked="checked" />
									<label class="form-check-label" for="price_choice_default"> by default</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="price_choice" value="min_period" id="price_choice_min_period" />
									<label class="form-check-label" for="price_choice_min_period"> by minimum period</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="price_choice" value="latest" id="price_choice_latest" />
									<label class="form-check-label" for="price_choice_latest"> by latest added</label>
								</div>
							</div>

							<div class="form-row">
								<input class="btn btn-primary" type="submit" name="submit" value="Create products item" />
								<a class="btn btn-secondary ml-2" href="<?php echo base_url(); ?>">Back to Catalog</a>
							</div>

						</form>

						<p class="mb-0">
						</p>

					</div>
				</div>
			</div>
		</div>
	</section>
