	<!-- PRICES
	============================================= -->
	<section class="section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="card p-4">

						<div class="mb-3">

							<!-- CREATE
							============================================= -->
							<?php echo form_open("admin/edit/$prod_id", array( 'class' => 'form-row', ) ) ?>

								<input type="hidden" name="action" value="price_create" />
								<input type="hidden" name="product_id" value="{prod_id}" />

								<div class="form-group">
									<label for="title">Price</label>
									<input class="form-control" type="input" name="price" value="" />
								</div>

								<div class="form-group ml-2">
									<label for="text">Period Begin</label>
									<input class="form-control" type="date" name="period_begin" value="" />
								</div>

								<div class="form-group ml-2">
									<label for="text">Period End</label>
									<input class="form-control" type="date" name="period_end" value="" />
								</div>

								<div class="form-group ml-2">
									<label for="text">Price Type</label><br />
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="type" value="regular" id="price_choice_regular" checked="checked" />
										<label class="form-check-label" for="price_choice_regular"> regular</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="type" value="sale" id="price_choice_sale" />
										<label class="form-check-label" for="price_choice_sale"> sale</label>
									</div>
								</div>

								<div class="form-group ml-2">
									<br>
									<input class="btn btn-sm btn-primary" type="submit" name="submit" value="Add Price" />
								</div>

							</form>

						</div>

						<table class="table table-hover mb-0">
							<thead>
								<tr>
									<th scope="col">ID</th>
									<th scope="col">Price</th>
									<th scope="col">Period Begin</th>
									<th scope="col">Period End</th>
									<th scope="col">Price Type</th>
									<th scope="col">Actions</th>
								</tr>
							</thead>
							<tbody>

							{products_prices}
								<tr>
									<th scope="row">{id}</th>
									<td>{price}</td>
									<td>{period_begin}</td>
									<td>{period_end}</td>
									<td>{type}</td>
									<td>
										<?php echo form_open("admin/edit/$prod_id", array( 'class' => 'form-row', ) ) ?>

											<input type="hidden" name="action" value="price_delete" />
											<input type="hidden" name="id" value="{id}" />

											<input class="btn btn-sm btn-danger" type="submit" name="submit" value="Delete" />

										</form>

									</td>
								</tr>
							{/products_prices}

							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</section>
