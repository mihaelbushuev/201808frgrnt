	<!-- PRICES
	============================================= -->
	<section class="section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="card p-4">

						<div class="mb-3">

							<!-- SETTINGS
							============================================= -->
							<?php echo form_open("admin/settings") ?>

							<input type="hidden" name="action" value="settings_edit" />
							<input type="hidden" name="name" value="{settings_name}" />

							<div class="form-group">
								<label for="title">Title</label>
								<input class="form-control" type="input" name="title" value="{settings_title}" disabled="" />
							</div>

							<div class="form-group">
								<label for="text">Price Choice Priority</label><br />
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="price_choice" value="min_period" id="price_choice_min_period" <?php echo ( $price_choice == 'min_period' ? 'checked="checked"' : '' ); ?> />
									<label class="form-check-label" for="price_choice_min_period"> by minimum period</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="price_choice" value="latest" id="price_choice_latest" <?php echo ( $price_choice == 'latest' ? 'checked="checked"' : '' ); ?> />
									<label class="form-check-label" for="price_choice_latest"> by latest added</label>
								</div>
							</div>

							<div class="form-row">
								<input class="btn btn-primary" type="submit" name="submit" value="Save Changes" />
								<a class="btn btn-secondary ml-2" href="<?php echo base_url(); ?>">Back to Catalog</a>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</section>
