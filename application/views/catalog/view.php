	<!-- ITEM
	============================================= -->
	<section class="section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="card p-4">

						<h3><b>Title:</b> {prod_title}</h3>

						<h5><b>Price:</b> {prod_price} руб.</h5>

				        <p class="mb-0">
				        	<a class="btn btn-success" href="admin/edit/{prod_id}">Edit Product</a>
				        	<a class="btn btn-primary" href="<?php echo base_url(); ?>">Back to Catalog</a>
				        </p>

					</div>
				</div>
			</div>
		</div>
	</section>
