	<!-- CATALOG
	============================================= -->
	<section class="section">
		<div class="container">
			<div class="row">

			{products}
				<div class="col-12 col-md-4 pb-4">
					<div class="card p-4">

						<h3>{title}</h3>

						<h5>{price} руб.</h5>

				        <p class="mb-0">
				        	<a class="btn btn-primary" href="{id}">View Product</a>
				        </p>

					</div>
				</div>
			{/products}

			</div>
		</div>
	</section>
