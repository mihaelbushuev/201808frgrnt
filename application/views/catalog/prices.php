	<link type="text/css" rel="StyleSheet" href="<?php echo base_url(); ?>assets/css/shieldui2016.css" />

	<!-- PRICES
	============================================= -->
	<section class="section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="card p-4">

						<div id="chart"></div>

					</div>
				</div>
			</div>
		</div>
	</section>

<script>
<?php
	$price_dates = $price_data = "";
	foreach ( $products_prices as $products_price ) {
		$price_dates .= "'" . date( 'd.m.Y', strtotime( $products_price['date'] ) ) . "',";
		$price_data .= $products_price['price'] . ",";
	}
?>
	var price_dates = [<?php echo $price_dates ?>];
	var price_data = [<?php echo $price_data ?>];
	var product_name = "{prod_title}";
</script>