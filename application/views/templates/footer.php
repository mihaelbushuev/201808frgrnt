
	<!-- FOOTER
	============================================= -->
	<footer class="footer py-4">

		<div class="footer-copy col-sm-12 text-center">
			Trading Platform &copy; <?php echo date('Y'); ?> All Rights Reserved.
		</div>	<!-- END footer-copy -->

	</footer>	<!-- END footer -->

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap-4.0.0-beta.3/js/bootstrap.min.js"></script>

<?php if ( isset( $shieldChart ) ) { ?>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/shieldui2016.js"></script>
	<script>
		$(function () {
			$('#chart').shieldChart({
				theme: 'light',
				seriesSettings: {
					line: {
						dataPointText: {
							enabled: true
						}
					}
				},
				chartLegend: {
					align: 'center',
					borderRadius: 2,
					borderWidth: 2,
					verticalAlign: 'top'
				},
				exportOptions: {
					image: true,
					print: true
				},
				axisX: {
					categoricalValues: price_dates
				},
				axisY: {
					title: {
						text: 'Цена (руб.)'
					}
				},
				primaryHeader: {
					text: ''
				},
				dataSeries: [
					{
						seriesType: 'line',
						collectionAlias: product_name,
						data: price_data
					}
				]
			});
		});
	</script>
<?php } ?>

</body>
</html>