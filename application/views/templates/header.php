<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="ru-RU">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="ru-RU">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="ru-RU">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="ru-RU">
<!--<![endif]-->
<head>

	<!-- Meta -->
	<meta charset="UTF-8" />
	<meta http-equiv="Content-Language" content="ru" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="{description}">
	<meta name="keywords" content="{keywords}" />

	<!-- Bootstrap -->
	<link rel='stylesheet' type='text/css' media='all' href='<?php echo base_url(); ?>assets/bootstrap-4.0.0-beta.3/css/bootstrap.min.css' />

	<!-- Title -->
	<title>{title} - Trading Platform</title>

</head>
<body>

	<!-- NAVBAR
	============================================= -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">

			<a class="navbar-brand" href="<?php echo base_url(); ?>">Trading Platform</a>

			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<ul class="navbar-nav">
					<li class="nav-item<?php echo ( current_url() == base_url() ? ' active' : '' ); ?>">
						<a class="nav-link" href="<?php echo base_url(); ?>">Catalog</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="AdminDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Admin
						</a>
						<div class="dropdown-menu" aria-labelledby="AdminDropdownMenu">
							<a class="dropdown-item" href="<?php echo site_url('admin/create'); ?>">Add Product</a>
							<a class="dropdown-item" href="<?php echo site_url('admin/settings'); ?>">Settings</a>
						</div>
					</li>
				</ul>
			</div>

		</div>
	</nav>

	<!-- TITLE
	============================================= -->
	<section class="section">
		<div class="container">
			<div class="row">
				<div class="col-12">

					<h1 class="my-3 text-center">{title}</h1>

				</div>
			</div>
		</div>
	</section>
