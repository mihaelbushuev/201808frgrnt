-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Авг 20 2018 г., 18:30
-- Версия сервера: 5.7.23-0ubuntu0.18.04.1
-- Версия PHP: 7.2.7-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `201808frgrnt`
--

-- --------------------------------------------------------

--
-- Структура таблицы `prices`
--

CREATE TABLE `prices` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `product_id` int(11) NOT NULL COMMENT 'ID продукта',
  `price` decimal(9,2) NOT NULL DEFAULT '0.00' COMMENT 'Цена',
  `period_begin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Начало периода действия цены',
  `period_end` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Конец периода действия цены',
  `type` varchar(128) NOT NULL DEFAULT 'regular' COMMENT 'Тип цены (обычная цена, скидка)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prices`
--

INSERT INTO `prices` (`id`, `product_id`, `price`, `period_begin`, `period_end`, `type`) VALUES
(1, 1, '15000.00', '2018-08-17 21:00:00', '2018-09-17 20:59:59', 'regular'),
(2, 1, '18000.00', '2018-08-17 21:00:00', '2019-08-17 20:59:59', 'regular');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `title` varchar(128) DEFAULT NULL COMMENT 'Наименование',
  `base_price` decimal(9,2) NOT NULL DEFAULT '0.00' COMMENT 'Базовая цена',
  `price_choice` varchar(128) NOT NULL DEFAULT 'default' COMMENT 'Способ определения цены'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `title`, `base_price`, `price_choice`) VALUES
(1, 'Школьная форма', '10000.00', 'default');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `name` varchar(128) DEFAULT NULL COMMENT 'Параметр',
  `title` varchar(128) DEFAULT NULL COMMENT 'Наименование',
  `value` varchar(256) DEFAULT NULL COMMENT 'Значение'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `name`, `title`, `value`) VALUES
(1, 'price_choice', 'Способ определения цены', 'min_period');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `prices`
--
ALTER TABLE `prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
